package backend;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import app.Film;

public class Backend {

	public static Elements initi() throws IOException {
		String pathToCsvFile = "C:\\Users\\kamil.kimlicka\\Downloads\\filmy.csv";
		Path path = Paths.get(pathToCsvFile);
		int sleepTimeout = 30;
		int checkTimeout = 0;
		LinkedList<Film> films = new LinkedList<Film>();
		Document doc;

		File fileSettings = new File("resources/settings.txt");
		BufferedReader brSettings = new BufferedReader(new FileReader(fileSettings));
		File fileUsers = new File("resources/users.txt");
		BufferedReader brUsers = new BufferedReader(new FileReader(fileUsers));
		String userString = brUsers.readLine();
		String proxyString = brSettings.readLine();

		if (proxyString != null) {
			doc = Jsoup.connect("https://www.imdb.com/user/" + userString + "/lists")
					.proxy(proxyString.split(":")[0], Integer.parseInt(proxyString.split(":")[1])) // sets a HTTP proxy
					.userAgent(
							"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2")
					.header("Content-Language", "en-US") //
					.get();
		}else {
			doc = Jsoup.connect("https://www.imdb.com/user/" + userString + "/lists").get();
		}

		Elements availableLists = new Elements();
		availableLists = doc.getElementsByClass("list-name");

		/*for (Element element : availableLists) {
			System.out.println(element.ownText() + " > " + element.attr("href"));
		}*/
		
		brUsers.close();
		brSettings.close();
		
		return availableLists;
	}
}
