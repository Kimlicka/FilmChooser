package app;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import backend.Backend;

import java.io.IOException;

import javax.swing.JCheckBox;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;

public class App {

	protected Shell shell;
	private Text text;
	private Text text_1;
	Group grpAddUser;
	Button loadButton;
	List filmList;
	Group grpWatchlists;
	Button[] checkBoxList;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			App window = new App();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 * 
	 * @throws Exception
	 */
	public void open() throws Exception {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		printList(filmList);
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}

	}

	/**
	 * Create contents of the window.
	 * 
	 * @throws IOException
	 */
	protected void createContents() throws IOException {
		shell = new Shell(SWT.SHELL_TRIM & (~SWT.RESIZE));
		shell.setSize(556, 424);
		shell.setText("What am I gonna watch tonight ?");
		shell.setLayout(new FormLayout());

		Label ListStateLabel = new Label(shell, SWT.NONE);
		FormData fd_ListStateLabel = new FormData();
		fd_ListStateLabel.right = new FormAttachment(0, 90);
		fd_ListStateLabel.top = new FormAttachment(0, 6);
		fd_ListStateLabel.left = new FormAttachment(0, 10);
		ListStateLabel.setLayoutData(fd_ListStateLabel);
		ListStateLabel.setText("New Label");

		loadButton = new Button(shell, SWT.NONE);
		FormData fd_loadButton = new FormData();
		fd_loadButton.bottom = new FormAttachment(0, 81);
		fd_loadButton.right = new FormAttachment(0, 90);
		fd_loadButton.top = new FormAttachment(0, 32);
		fd_loadButton.left = new FormAttachment(0, 10);
		loadButton.setLayoutData(fd_loadButton);
		loadButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				grpAddUser.setVisible(true);
				loadButton.setEnabled(false);
			}
		});
		loadButton.setText("Load User");

		filmList = new List(shell, SWT.BORDER);
		FormData fd_filmList = new FormData();
		fd_filmList.bottom = new FormAttachment(0, 283);
		fd_filmList.right = new FormAttachment(0, 524);
		fd_filmList.top = new FormAttachment(0, 34);
		fd_filmList.left = new FormAttachment(0, 294);
		filmList.setLayoutData(fd_filmList);
		// filmList.setVisible(false);

		Button chooseButton = new Button(shell, SWT.NONE);
		FormData fd_chooseButton = new FormData();
		fd_chooseButton.bottom = new FormAttachment(0, 352);
		fd_chooseButton.right = new FormAttachment(0, 219);
		fd_chooseButton.top = new FormAttachment(0, 298);
		fd_chooseButton.left = new FormAttachment(0, 80);
		chooseButton.setLayoutData(fd_chooseButton);
		chooseButton.setText("Choose a Movie !");
		chooseButton.setEnabled(false);

		grpWatchlists = new Group(shell, SWT.NONE);
		FormData fd_grpWatchlists = new FormData();
		fd_grpWatchlists.bottom = new FormAttachment(0, 282);
		fd_grpWatchlists.right = new FormAttachment(0, 278);
		fd_grpWatchlists.top = new FormAttachment(0, 87);
		fd_grpWatchlists.left = new FormAttachment(0, 10);
		grpWatchlists.setLayoutData(fd_grpWatchlists);
		grpWatchlists.setText("Watchlists");
		grpWatchlists.setLayout(new GridLayout(1, false));

		text = new Text(shell, SWT.BORDER);
		FormData fd_text = new FormData();
		fd_text.bottom = new FormAttachment(0, 308);
		fd_text.right = new FormAttachment(0, 524);
		fd_text.top = new FormAttachment(0, 283);
		fd_text.left = new FormAttachment(0, 294);
		text.setLayoutData(fd_text);
		text.setVisible(false);

		Button showPageButton = new Button(shell, SWT.NONE);
		FormData fd_showPageButton = new FormData();
		fd_showPageButton.bottom = new FormAttachment(0, 347);
		fd_showPageButton.right = new FormAttachment(0, 470);
		fd_showPageButton.top = new FormAttachment(0, 304);
		fd_showPageButton.left = new FormAttachment(0, 342);
		showPageButton.setLayoutData(fd_showPageButton);
		showPageButton.setText("Go to Web Page");
		showPageButton.setEnabled(false);

		grpAddUser = new Group(shell, SWT.NONE);
		FormData fd_grpAddUser = new FormData();
		fd_grpAddUser.bottom = new FormAttachment(0, 81);
		fd_grpAddUser.right = new FormAttachment(0, 278);
		fd_grpAddUser.top = new FormAttachment(0, 11);
		fd_grpAddUser.left = new FormAttachment(0, 104);
		grpAddUser.setLayoutData(fd_grpAddUser);
		grpAddUser.setText("Add User ID");
		grpAddUser.setVisible(false);

		text_1 = new Text(grpAddUser, SWT.BORDER);
		text_1.setBounds(10, 30, 76, 21);

		Button submitUserButton = new Button(grpAddUser, SWT.NONE);
		submitUserButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				grpAddUser.setVisible(false);
				loadButton.setEnabled(true);
			}
		});
		submitUserButton.setBounds(103, 28, 61, 25);
		submitUserButton.setText("Submit");

		Menu menu = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menu);

		MenuItem SettingsMenu = new MenuItem(menu, SWT.CASCADE);
		SettingsMenu.setText("Settings");

		Menu settings_menu_1 = new Menu(SettingsMenu);
		SettingsMenu.setMenu(settings_menu_1);

		MenuItem settings_proxy = new MenuItem(settings_menu_1, SWT.NONE);
		settings_proxy.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ProxySettings proxyDialog = new ProxySettings(shell, 0);
				proxyDialog.open();
			}
		});
		settings_proxy.setText("Proxy settings");

		MenuItem Exitmenu = new MenuItem(menu, SWT.NONE);
		Exitmenu.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				shell.close();
			}
		});
		Exitmenu.setText("Exit");

	}

	private void printList(List filmList) throws Exception {
		filmList.setVisible(true);
		Elements es = Backend.initi();
		int i = 0;
		int margin = 30;
		checkBoxList = new Button[es.size()];
		for (Element element : es) {
			//filmList.add(element.ownText() + " > " + element.attr("href"));
			createCheckBoxes(element.ownText(), i, margin);
			i++;
			margin=+margin;
		}
	}

	private void createCheckBoxes(String label, int n, int margin) {
		checkBoxList[n] = new Button(grpWatchlists, SWT.CHECK);
		checkBoxList[n].setText(label);
		checkBoxList[n].setBounds(10, margin, 200, margin);
	}
}
