package app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Main {

	public static void main(String[] args) throws MalformedURLException, IOException, InterruptedException {

		String pathToCsvFile = "C:\\Users\\kamil.kimlicka\\Downloads\\filmy.csv";
		Path path = Paths.get(pathToCsvFile);
		int sleepTimeout = 30;
		int checkTimeout = 0;
		LinkedList<Film> films = new LinkedList<Film>();
		Document doc;

		File fileSettings = new File("resources/settings.txt");
		BufferedReader brSettings = new BufferedReader(new FileReader(fileSettings));
		File fileUsers = new File("resources/users.txt");
		BufferedReader brUsers = new BufferedReader(new FileReader(fileUsers));
		String userString = brUsers.readLine();
		String proxyString = brSettings.readLine();

		if (proxyString != null) {
			doc = Jsoup.connect("https://www.imdb.com/user/" + userString + "/lists")
					.proxy(proxyString.split(":")[0], Integer.parseInt(proxyString.split(":")[1])) // sets a HTTP proxy
					.userAgent(
							"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2")
					.header("Content-Language", "en-US") //
					.get();
		}else {
			doc = Jsoup.connect("https://www.imdb.com/user/" + userString + "/lists").get();
		}

		Elements availableLists = new Elements();
		availableLists = doc.getElementsByClass("list-name");

		for (Element element : availableLists) {
			System.out.println(element.ownText() + " > " + element.attr("href"));
		}

		/*
		 * Runtime.getRuntime().exec( new String[] { "cmd", "/c",
		 * "start chrome http://www.imdb.com/list/ls054817306/export?ref_=ttls_exp" });
		 * 
		 * while (!Files.exists(path) && checkTimeout <= sleepTimeout) {
		 * Thread.sleep(1000); checkTimeout++; }
		 * 
		 * Reader reader = new FileReader(pathToCsvFile);
		 * 
		 * Iterable<CSVRecord> records = CSVFormat.RFC4180.withHeader("Position",
		 * "Const", "Created", "Modified", "Description", "Title", "URL", "Title Type",
		 * "IMDb Rating", "Runtime (mins)", "Year", "Genres", "Num Votes",
		 * "Release Date", "Directors", "Your Rating", "Date Rated").parse(reader); for
		 * (CSVRecord record : records) { films.add(new Film(record.get("Title"),
		 * record.get("URL"))); }
		 * 
		 * reader.close();
		 * 
		 * try { Files.deleteIfExists(Paths.get(pathToCsvFile)); } catch
		 * (NoSuchFileException e) {
		 * System.out.println("No such file/directory exists"); } catch
		 * (DirectoryNotEmptyException e) {
		 * System.out.println("Directory is not empty."); } catch (IOException e) {
		 * System.out.println("Invalid permissions."); }
		 * 
		 * System.out.println("Deletion successful.");
		 */
	}

}
