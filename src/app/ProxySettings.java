package app;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

public class ProxySettings extends Dialog {

	protected Object result;
	protected Shell shell;
	private Text txtD;
	private Text text_1;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public ProxySettings(Shell parent, int style) {
		super(parent, style);
		setText("Proxy Settings");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(SWT.APPLICATION_MODAL);
		shell.setSize(132, 211);
		shell.setText(getText());
		shell.setLocation(getParent().getLocation());
		
		Group grpEnterProxy = new Group(shell, SWT.NONE);
		grpEnterProxy.setText("Enter proxy");
		grpEnterProxy.setBounds(10, 10, 106, 113);
		
		txtD = new Text(grpEnterProxy, SWT.BORDER);
		txtD.setLocation(10, 46);
		txtD.setSize(86, 21);
		
		text_1 = new Text(grpEnterProxy, SWT.BORDER);
		text_1.setLocation(10, 87);
		text_1.setSize(86, 21);
		
		Label lblNewLabel = new Label(grpEnterProxy, SWT.NONE);
		lblNewLabel.setBounds(10, 31, 86, 15);
		lblNewLabel.setText("Proxy address:");
		
		Label lblNewLabel_1 = new Label(grpEnterProxy, SWT.NONE);
		lblNewLabel_1.setBounds(10, 71, 75, 15);
		lblNewLabel_1.setText("Port number:");
		
		Button ProxySubmitButton = new Button(shell, SWT.NONE);
		ProxySubmitButton.setLocation(31, 135);
		ProxySubmitButton.setSize(64, 29);
		ProxySubmitButton.setText("Submit");
		
		Button ProxyCancelButton = new Button(shell, SWT.NONE);
		ProxyCancelButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				shell.dispose();
			}
		});
		ProxyCancelButton.setBounds(31, 170, 64, 29);
		ProxyCancelButton.setText("Cancel");

	}
}
