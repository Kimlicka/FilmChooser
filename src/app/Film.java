package app;

public class Film {
	
	String title;
	String url;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Film(String title, String url) {
		super();
		this.title = title;
		this.url = url;
	}
}
